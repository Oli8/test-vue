import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/Login'
import Home from '@/components/Home'
import store from '../store';

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
		meta: { requiresAuth: true }
  },
	{
		path: '/login',
		name: 'login',
		component: Login
	},
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
	if(to.meta.requiresAuth && !store.getters.isAuthenticated){
		next({path: '/login'})
	} else {
		next()
	}
});


export default router
