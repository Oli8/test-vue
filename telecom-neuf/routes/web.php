<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Response;

$router->post('/auth', function(\Illuminate\Http\Request $request) {
    $users = [
        [
            'email' => 'user@test.com',
            'password' => 'abc123',
        ],
    ];


    $data = $request->json()->all();
       

    if (in_array([
            'email' => $data["email"],
            'password' => $data["password"],
        ], $users)) {
        return new Response('Login successful', 200);
    } 
        
    return new Response('An error has occurred', 400);
});

